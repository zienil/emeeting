## About eManagement System (eMs)
1. Login using username/email
2. Plugin : Dropzone File Uplod
3. Interface : CoreUI
4. JQuery + JQuery UI
5. Table : Datatables
6. Editor : WYSIWYG Editor
7. SPatie Auth

---
### NOTE

---
### Installation Instructions
1. Run clone
    - `git clone `
2. Run `npm install`
3. Run `composer install` (To create Vendor folder)
4. Create a MySQL database for the project
    - `mysql -u root -p`
    - `create database <dbname>;`
    - `exit`
5. From the projects root run :
    - `cp .env.example .env`
6. Configure your `.env` file
7. From the projects root folder run :
    - `php artisan key:generate`
    - `php artisan migrate`
    - `composer dump-autoload`
    - `php artisan db:seed`
    - `php artisan passport:install`
    
### Rolling Back Migrations
1. Rollback The Last Migration Operation :
    - `php artisan migrate:rollback`
2. Rollback all migrations :
    - `php artisan migrate:reset`
3. Rollback all migrations and run them all again :
    - `php artisan migrate:refresh`
    - `php artisan migrate:refresh --seed`

### Laravel Artisan Command
1. Check Laravel Version :
    - `php artisan --version`
2. Re-optimized class loader :
    - `php artisan optimize:clear`
3. Clear Cache facade value :
    - `php artisan cache:clear`
4. Clear Route cache :
    - `php artisan route:clear`
5. Clear View cache :
    - `php artisan view:clear`
6. Clear Config cache :
    - `php artisan config:clear`
7. Build Cache :
    - `php artisan config:cache`
8. After delete controller :
    - `composer dump-autoload -o`
9. Create Controller with resource and model :
    - `php artisan make:controller TestController -r -m`
10. Make model with migration :
    - `php artisan make:model Model_Name -m`
11. Maintenance mode on : `php artisan down`
12. Maintenance mode off : `php artisan up`
13. To create the symbolic link between storage folder and public folder : `php artisan storage:link`


### Authentication Quickstart
1. Run : `composer require laravel/ui`
2. Run : `php artisan ui bootstrap --auth`
3. Compile fresh scaffolding : `npm install && npm run dev`

### Laravel File System
1. Storage : Public Disc
   1. create the symbolic link : `php artisan storage:link`

### Special Laravel Artisan Command
1. Run Certain Seeder (To update Permission List) : `php artisan db:seed --class=PermissionTableSeeder`
2. Laravel run specific migration :
    - `php artisan migrate --path=/database/migrations/my_migration.php`
    - `php artisan migrate:refresh --path=/database/migrations/fileName.php`
    - `php artisan migrate:rollback --step=1/2/3`


## Build the Front End Assets with Mix

---

### Using NPM :
1. Provides live reloading. This should be used for development only : `npm run dev`
2. Run scripts from package.json when files change : `npm run watch`
3. If `npm run watch` don't work :
    - remove node_modules :
        - `rm -rf node_modules && npm install`

### Using Yarn:

1. From the projects root folder run  : `yarn install`
2. From the projects root folder run :
    - `yarn run dev` or
    - `yarn run production`

* You can watch assets with : `yarn run watch`


## Plugin Installation

---
1. Bootstrap 5.13 : `npm i bootstrap`
2. PopperJS@core : `npm i @popperjs/core`
3. Hover.css : `npm i hover.css --save`
4. Dropzone : `npm i dropzone`
5. JQuery UI : `npm i jquery-ui`

### Intervention PHP Image
http://image.intervention.io/getting_started/installation
1. `composer require intervention/image`

### Spatie Tutorial
https://www.itsolutionstuff.com/post/laravel-8-user-roles-and-permissions-tutorialexample.html
1. Install Composer Packages
   1. `composer require spatie/laravel-permission`
   2. `composer require laravelcollective/html`
2. `config/app.php`

   ```
   'providers' => [Spatie\Permission\PermissionServiceProvider::class,]

3. `php artisan vendor:publish --provider="Spatie\Permission\PermissionServiceProvider"`
4. `php artisan migrate`
5. Add Middleware : `app/Http/Kernel.php`
   ````
   protected $routeMiddleware = [
    'role' => \Spatie\Permission\Middlewares\RoleMiddleware::class,
    'permission' => \Spatie\Permission\Middlewares\PermissionMiddleware::class,
    'role_or_permission' => \Spatie\Permission\Middlewares\RoleOrPermissionMiddleware::class,]


### POSTGRESQL
https://tonyfrenzy.medium.com/using-postgresql-with-laravel-c4c320ca7f34

### InvalidArgumentException  :

    Please provide a valid cache path.

Need to create bellow folder inside storage folder

    -   framework
            -   cache
            -   sessions
            -   views
