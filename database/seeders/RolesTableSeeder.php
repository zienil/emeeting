<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::where('name', '=', 'system admin')->first() === null) {
            Role::create([
                'name'       => 'system admin',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'system support')->first() === null) {
            Role::create([
                'name'       => 'system support',
                'guard_name' => 'web',
            ]);
        }
    }
}
