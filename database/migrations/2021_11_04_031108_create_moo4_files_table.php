<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoo4FilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moo4_files', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('moo3AgendaID')->unsigned()->index()->comment('Table moo3Agenda Id');
            $table->string('moo4FileName');
            $table->string('moo4FileType');
            $table->foreign('moo3AgendaID')->references('id')->on('moo3_agendas')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moo4_files');
    }
}
