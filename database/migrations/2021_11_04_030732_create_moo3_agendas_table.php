<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMoo3AgendasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moo3_agendas', function (Blueprint $table) {
            $table->id();
            $table->string('moo3AgendaName');
            $table->text('moo3AgendaDesc');
            $table->string('created_by');
            $table->string('updated_by')->nullable();
            $table->unsignedBigInteger('moo2MeetingID')->unsigned()->index()->comment('Table moo2Meeting Id');
            $table->foreign('moo2MeetingID')->references('id')->on('moo2_meetings')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moo3_agendas');
    }
}
