<?php

namespace App\Http\Controllers;

use App\Models\MOO2Meeting;
use Illuminate\Http\Request;

class Moo2MeetingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('meetings.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MOO2Meeting  $mOO2Meeting
     * @return \Illuminate\Http\Response
     */
    public function show(MOO2Meeting $mOO2Meeting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MOO2Meeting  $mOO2Meeting
     * @return \Illuminate\Http\Response
     */
    public function edit(MOO2Meeting $mOO2Meeting)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\MOO2Meeting  $mOO2Meeting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, MOO2Meeting $mOO2Meeting)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MOO2Meeting  $mOO2Meeting
     * @return \Illuminate\Http\Response
     */
    public function destroy(MOO2Meeting $mOO2Meeting)
    {
        //
    }
}
