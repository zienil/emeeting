<?php

namespace App\Http\Controllers;

use App\Models\Moo3Agenda;
use Illuminate\Http\Request;

class Moo3AgendaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Moo3Agenda  $moo3Agenda
     * @return \Illuminate\Http\Response
     */
    public function show(Moo3Agenda $moo3Agenda)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Moo3Agenda  $moo3Agenda
     * @return \Illuminate\Http\Response
     */
    public function edit(Moo3Agenda $moo3Agenda)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Moo3Agenda  $moo3Agenda
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Moo3Agenda $moo3Agenda)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Moo3Agenda  $moo3Agenda
     * @return \Illuminate\Http\Response
     */
    public function destroy(Moo3Agenda $moo3Agenda)
    {
        //
    }
}
