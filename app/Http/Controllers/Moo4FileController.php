<?php

namespace App\Http\Controllers;

use App\Models\Moo4File;
use Illuminate\Http\Request;

class Moo4FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Moo4File  $moo4File
     * @return \Illuminate\Http\Response
     */
    public function show(Moo4File $moo4File)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Moo4File  $moo4File
     * @return \Illuminate\Http\Response
     */
    public function edit(Moo4File $moo4File)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Moo4File  $moo4File
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Moo4File $moo4File)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Moo4File  $moo4File
     * @return \Illuminate\Http\Response
     */
    public function destroy(Moo4File $moo4File)
    {
        //
    }
}
