/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
try {
    window.$ = window.jQuery = require('jquery');
    require('@popperjs/core');
    require('./bootstrap');
    // require('@coreui/coreui');
    // require('pace-progress');
    require('perfect-scrollbar');
    // var Dropzone = require('dropzone');
} catch (e) {
}

import 'jquery-ui/ui/widgets/datepicker.js';
