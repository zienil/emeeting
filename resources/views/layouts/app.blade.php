<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="eManagementSystem_eMs_V1">
    <meta name="author" content="theZie">
    <link rel="shortcut icon" href="#">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@hasSection('template_title')@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" rel="stylesheet">


    {{--CKEDITOR5--}}
    <script src="{{asset('/plugins/ckeditor/ckeditor.js')}}"></script>
    @yield('third_party_stylesheets')
    @yield('head')
</head>
<body>
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    @include('partials.header')
</header>

<div class="container-fluid">
    <div class="row">
        <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
            @include('partials.sidebar')
        </nav>
        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4 pt-3 pb-2 mb-3">
            @yield('content')
        </main>
    </div>
</div>
<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
{{-- Scripts --}}
<script>
    window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
</script>
@yield('third_party_scripts')
@stack('page_scripts')
</body>
</html>
