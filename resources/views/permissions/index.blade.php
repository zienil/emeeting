@extends('layouts.app')

@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('permissions.index') }}">Permissions</a></li>
        </ol>
    </div>
@stop
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Permission Management
                <div class="float-end">
                    <form method="GET" action="{{ route('permissions.index') }}">
                        <div class="input-group">
                            <input class="form-control form-control-sm" type="text" name="keyword"
                                   placeholder="Search...."
                                   value="{{ request()->get('keyword') }}">
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-sm" type="submit"><i class="fas fa-search"></i>
                                    Search
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </h5>
            <div class="card-body">
                <div class="pull-right">
                    @can('permission-create')
                        <a class="btn btn-sm btn-success" href="{{ route('permissions.create') }}">
                            <i class="fas fa-plus-circle"></i> Create New Permission</a>
                    @endcan
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success mt-2">
                        <p>{{ $message }}</p>
                    </div>
                @endif
                <div class="float-md-right mb-1">
                    <span class="badge badge-warning">Total Permissions : {{$permissions->total()}}</span>
                </div>

                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-sm table-hover">
                        <thead class="thead-dark text-center">
                        <tr>
                            <th colspan="align-middle">#</th>
                            <th colspan="align-middle">Name</th>
                            <th width="280px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($permissions as $key => $permission)
                            <tr class="text-center">
                                <th class="align-middle"
                                    scope="row">{{ ($permissions ->currentpage()-1) * $permissions ->perpage() + $loop->index + 1 }}</th>
                                <td class="align-middle">{{ $permission->name }}</td>
                                <td class="align-middle">
                                    <a class="btn btn-sm btn-info"
                                       href="{{ route('permissions.show',$permission->id) }}">
                                        <i class="fas fa-info-circle"></i> Show</a>
                                    @can('permission-edit')
                                        <a class="btn btn-sm btn-primary"
                                           href="{{ route('permissions.edit',$permission->id) }}">
                                            <i class="fas fa-pen-square"></i> Edit</a>
                                    @endcan
                                    @can('permission-delete')
                                        {!! Form::open(['method' => 'DELETE','onsubmit' => "return confirm('Are you sure you want to delete?');",'route' => ['permissions.destroy', $permission->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="float-right">
                    {{ $permissions->appends(['keyword'=> request()->get('keyword')])->links() }}
                </div>
            </div>
        </div>
    </div>

@stop
