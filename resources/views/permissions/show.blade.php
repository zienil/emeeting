@extends('layouts.app')

@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('permissions.index') }}">Permissions</a></li>
        </ol>
    </div>
@stop
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Show Permission Details
                <div class="float-end">
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('permissions.index') }}"><i class="fas fa-chevron-circle-left"></i> Back</a>
                </div>
            </h5>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-3">Permission Name</dt>
                    <dd class="col-sm-9">{{ $permission->name }}</dd>
                </dl>
            </div>
        </div>
    </div>
@stop
