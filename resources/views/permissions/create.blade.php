@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('permissions.index') }}">Permissions</a></li>
        </ol>
    </div>
@stop
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Create New Permission
                <div class="float-end">
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('permissions.index') }}"><i class="fas fa-chevron-circle-left"></i> Back</a>
                </div>
            </h5>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger mt-2">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(array('route' => 'permissions.store','method'=>'POST')) !!}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Permission Name</label>
                            <div class="col-sm-10">
                                {!! Form::text('name', null, array('placeholder' => 'Permission Name','class' => 'form-control form-control-sm')) !!}
                            </div>
                        </div>
                        <button type="submit" class="btn btn-sm btn-primary float-right"><i class="fas fa-save"></i> Submit</button>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop
