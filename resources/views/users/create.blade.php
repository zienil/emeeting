@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header"> Create New User
            <div class="float-end">
                <a class="btn btn-outline-dark btn-sm" href="{{ route('users.index') }}"> Back</a>
            </div>
        </h5>
        {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Username</label>
                <div class="col-sm-10">
                    {!! Form::text('username', null, array('placeholder' => 'Username' ,'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Fullname</label>
                <div class="col-sm-10">
                    {!! Form::text('fullname', null, array('placeholder' => 'Fullname','class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Email</label>
                <div class="col-sm-10">
                    {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Password</label>
                <div class="col-sm-10">
                    {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Confirm Password</label>
                <div class="col-sm-10">
                    {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">Role</label>
                <div class="col-sm-10">
                    {!! Form::select('roles', $roles,[], array('class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
        </div>
        <div class="card-footer text-right">
            <button type="submit" class="btn btn-sm btn-primary">Submit</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection
