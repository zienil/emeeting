@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            <i class='fas fa-users-cog mr-2'></i> User Management
            @if(Auth::user()->can('user-create'))
                <div class="float-end">
                    <a class="btn btn-sm btn-success"
                       href="{{ route('users.create') }}"> New User</a>
                </div>
            @endif
        </h5>
        <div class="card-body">
            <div class="table-responsive">
                <table id="userList" class="table table-sm table-bordered table-striped">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th scope="col" class="align-middle">#</th>
                        <th scope="col" class="align-middle">Full Name</th>
                        <th scope="col" class="align-middle">Email</th>
                        <th scope="col" class="align-middle">Role</th>
                        <th scope="col" class="align-middle">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $user)
                        <tr class="text-center">
                            <td class="align-middle">{{ ($data ->currentpage()-1) * $data ->perpage() + $loop->index + 1 }}</td>
                            <td class="align-middle">{{ $user->fullname }}</td>
                            <td class="align-middle">{{ $user->email }}</td>
                            <td class="align-middle">
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                        <span class="badge bg-success">{{ $v }}</span>
                                    @endforeach
                                @endif
                            </td>
                            <td class="align-middle">
                                <a class="btn btn-sm btn-info" href="{{ route('users.show',$user->id) }}">Show</a>
                                @if(Auth::user()->can('user-edit'))
                                    <a class="btn btn-sm btn-primary" href="{{ route('users.edit',$user->id) }}">Edit</a>
                                @endif
                                @if(Auth::user()->can('user-delete'))
                                    {!! Form::open(array('route' => ['users.destroy', $user->id], 'title' => 'Delete','style'=>'display:inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::button('Delete', array('class' => 'btn btn-danger btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => 'Delete')) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    {{--    @include('modals.modal-delete')--}}
@endsection
@section('third_party_stylesheets')
    <link rel="stylesheet" href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    {{--    @include('scripts.datatables')--}}
    {{--    @include('scripts.delete-modal-script')--}}
@stop
