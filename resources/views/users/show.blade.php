@extends('layouts.app')


@section('content')
    <div class="card">
        <h5 class="card-header">Show User : {{$user->username}}
            <div class="float-end">
                <a class="btn btn-outline-dark btn-sm" href="{{ route('users.index') }}"> Back</a>
            </div>
        </h5>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">Username</dt>
                <dd class="col-sm-9">{{ $user->username }}</dd>
                <dt class="col-sm-3">Full Name</dt>
                <dd class="col-sm-9">{{ $user->fullname }}</dd>
                <dt class="col-sm-3">Email</dt>
                <dd class="col-sm-9">{{ $user->email }}</dd>
                <dt class="col-sm-3">Role</dt>
                <dd class="col-sm-9">
                    @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $v)
                            <span class="badge bg-success">{{ $v }}</span>
                        @endforeach
                    @endif
                </dd>
            </dl>
        </div>
    </div>
@endsection
