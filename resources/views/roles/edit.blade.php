@extends('layouts.app')

@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">Roles</a></li>
        </ol>
    </div>
@stop

@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Edit Role
                <div class="float-end">
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('roles.index') }}"><i class="fas fa-chevron-circle-left"></i> Back</a>
                </div>
            </h5>
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger mt-2">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Role Name</label>
                    <div class="col-sm-10">
                        {!! Form::text('name', null, array('placeholder' => 'Role Name','class' => 'form-control form-control-sm')) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Permissions</label>
                    <div class="col-sm-10">
                        @foreach($permission as $value)
                            <div class="form-check">
                                {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'form-check-input')) }}
                                <label class="form-check-label">{{ $value->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <button type="submit" class="btn btn-sm btn-primary float-right"><i class="fas fa-save"></i> Submit</button>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop

