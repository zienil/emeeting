@extends('layouts.app')

@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">Roles</a></li>
        </ol>
    </div>
@stop
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Show Role Details
                <div class="float-end">
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('roles.index') }}"><i class="fas fa-chevron-circle-left"></i> Back</a>
                </div>
            </h5>
            <div class="card-body">
                <dl class="row">
                    <dt class="col-sm-3">Role Name</dt>
                    <dd class="col-sm-9">{{ $role->name }}</dd>
                    <dt class="col-sm-3">Role Permissions</dt>
                    <dd class="col-sm-9">
                        @if(!empty($rolePermissions))
                            <ul class="list-inline">
                                @foreach($rolePermissions as $v)
                                    <li class="list-inline-item"><span class="badge bg-primary">{{ $v->name }}</span></li>
                                @endforeach
                            </ul>
                        @else
                            <span class="badge bg-secondary">No Permissions</span>
                        @endif
                    </dd>
                </dl>
            </div>
        </div>
    </div>
@stop
