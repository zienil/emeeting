@extends('layouts.app')

@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">Roles</a></li>
        </ol>
    </div>
@stop
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Role Management
                <div class="float-end">
                    <form method="GET" action="{{ route('roles.index') }}">
                        <div class="input-group">
                            <input class="form-control form-control-sm" type="text" name="keyword"
                                   placeholder="Search...."
                                   value="{{ request()->get('keyword') }}">
                            <div class="input-group-append">
                                <button class="btn btn-primary btn-sm" type="submit"><i class="fas fa-search"></i> Search</button>
                            </div>
                        </div>
                    </form>
                </div>
            </h5>
            <div class="card-body">
                <div class="pull-right">
                    @can('role-create')
                        <a class="btn btn-sm btn-success" href="{{ route('roles.create') }}">
                            <i class="fas fa-plus-circle"></i> Create New Role</a>
                    @endcan
                </div>
                @if ($message = Session::get('success'))
                    <div class="alert alert-success mt-2">
                        <p>{{ $message }}</p>
                    </div>
                @endif

                <div class="float-md-right mb-1">
                    <span class="badge badge-warning">Total Roles : {{$roles->total()}}</span>
                </div>

                <div class="table-responsive">
                    <table class="table table-sm table-bordered table-sm table-hover">
                        <thead class="thead-dark text-center">
                        <tr>
                            <th class="align-middle">#</th>
                            <th class="align-middle">Name</th>
                            <th width="280px">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($roles as $key => $role)
                            <tr class="text-center">
                                <th scope="row">{{ ($roles ->currentpage()-1) * $roles ->perpage() + $loop->index + 1 }}</th>
                                <td class="align-middle">{{ $role->name }}</td>
                                <td class="align-middle">
                                    <a class="btn btn-sm btn-info" href="{{ route('roles.show',$role->id) }}">
                                        <i class="fas fa-info-circle"></i> Show</a>
{{--                                    @can('role-edit')--}}
                                        <a class="btn btn-sm btn-primary" href="{{ route('roles.edit',$role->id) }}">
                                            <i class="fas fa-pen-square"></i> Edit</a>
{{--                                    @endcan--}}
                                    @can('role-delete')
                                        {!! Form::open(['method' => 'DELETE','onsubmit' => "return confirm('Are you sure you want to delete?');",'route' => ['roles.destroy', $role->id],'style'=>'display:inline']) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-sm btn-danger']) !!}
                                        {!! Form::close() !!}
                                    @endcan
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                {{ $roles->appends(['keyword'=> request()->get('keyword')])->links() }}
            </div>
        </div>
    </div>
@stop
