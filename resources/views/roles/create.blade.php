@extends('layouts.app')
@section('breadcrumb')
    <div class="c-subheader px-3">
        <ol class="breadcrumb border-0 m-0">
            <li class="breadcrumb-item">Home</li>
            <li class="breadcrumb-item">Authentication</li>
            <li class="breadcrumb-item active"><a href="{{ route('roles.index') }}">Roles</a></li>
        </ol>
    </div>
@stop
@section('content')
    <div class="container-fluid">
        <div class="card border-primary">
            <h5 class="card-header text-uppercase">
                Create New Role
                <div class="float-end">
                    <a class="btn btn-sm btn-outline-dark" href="{{ route('roles.index') }}"><i class="fas fa-chevron-circle-left"></i> Back</a>
                </div>
            </h5>
            <div class="card-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger mt-2">
                        <strong>Whoops!</strong> There were some problems with your input.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Role Name</label>
                    <div class="col-sm-10">
                        {!! Form::text('name', null, array('placeholder' => 'Role Name','class' => 'form-control form-control-sm')) !!}
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Permissions</label>
                    <div class="col-sm-10">
                        @foreach($permission as $value)
                            <div class="form-check">
                                {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'form-check-input')) }}
                                <label class="form-check-label">{{ $value->name }}</label>
                            </div>
                        @endforeach
                    </div>
                </div>
                <button type="submit" class="btn btn-sm btn-primary float-right"><i class="fas fa-save"></i> Submit</button>

                {{--                <div class="row">--}}
                {{--                    <div class="col-xs-12 col-sm-12 col-md-12">--}}
                {{--                        <div class="form-group">--}}
                {{--                            <strong>Name:</strong>--}}
                {{--                            {!! Form::text('name', null, array('placeholder' => 'Name','class' => 'form-control')) !!}--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="col-xs-12 col-sm-12 col-md-12">--}}
                {{--                        <div class="form-group">--}}
                {{--                            <strong>Permission:</strong>--}}
                {{--                            <br/>--}}
                {{--                            @foreach($permission as $value)--}}
                {{--                                <label>{{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}--}}
                {{--                                    {{ $value->name }}</label>--}}
                {{--                                <br/>--}}
                {{--                            @endforeach--}}
                {{--                        </div>--}}
                {{--                    </div>--}}
                {{--                    <div class="col-xs-12 col-sm-12 col-md-12 text-center">--}}
                {{--                        <button type="submit" class="btn btn-primary">Submit</button>--}}
                {{--                    </div>--}}
                {{--                </div>--}}
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop
