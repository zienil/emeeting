<a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="/home">eMeeting</a>
<button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
</button>
<div class="navbar-nav">
    <div class="nav-item text-nowrap">
{{--        <a class="nav-link px-3" href="#">Sign out</a>--}}
        @guest
            @if (Route::has('login'))
                    <a class="nav-link px-3" href="{{ route('login') }}">{{ __('Login') }}</a>

            @endif

            @if (Route::has('register'))
                    <a class="nav-link px-3" href="{{ route('register') }}">{{ __('Register') }}</a>

            @endif
        @else
{{--            <li class="nav-item dropdown">--}}
{{--                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>--}}
{{--                    {{ Auth::user()->name }}--}}
{{--                </a>--}}

{{--                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">--}}
                    <a class="nav-link px-3" href="{{ route('logout') }}"
                       onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
{{--                </div>--}}
{{--            </li>--}}
        @endguest
    </div>
</div>
