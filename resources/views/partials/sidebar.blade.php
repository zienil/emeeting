<div class="position-sticky pt-3">
    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
        <span>eMeeting Properties</span>
    </h6>
    <ul class="nav flex-column">
        <li class="nav-item">
            <a class="nav-link" aria-current="page" href="{{route('meetings.index')}}">
                <span data-feather="home"></span>
                Manage Meeting
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="#">
                <span data-feather="file"></span>
                Orders
            </a>
        </li>
    </ul>

    @if(Auth::user()->hasRole('system admin'))
        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
            <span>Site Administrator</span>
        </h6>
        <ul class="nav flex-column mb-2">
            @if(Auth::user()->can('user-list'))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('users.index')}}">
                        <span data-feather="file-text"></span>
                        Users
                    </a>
                </li>
            @endif
            @if(Auth::user()->can('role-list'))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('roles.index')}}">
                        <span data-feather="file-text"></span>
                        Roles
                    </a>
                </li>
            @endif
            @if(Auth::user()->can('permission-list'))
                <li class="nav-item">
                    <a class="nav-link" href="{{route('permissions.index')}}">
                        <span data-feather="file-text"></span>
                        Permissions
                    </a>
                </li>
            @endif
        </ul>
    @endif
</div>
