@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            <i class='fas fa-users-cog mr-2'></i> User Management
            @if(Auth::user()->can('user-create'))
                <div class="float-end">
                    <a class="btn btn-sm btn-success"
                       href="{{ route('users.create') }}"> New User</a>
                </div>
            @endif
        </h5>
        <div class="card-body">
            <div class="table-responsive">
            </div>
        </div>
    </div>
    {{--    @include('modals.modal-delete')--}}
@endsection
@section('third_party_stylesheets')
    <link rel="stylesheet" href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    {{--    @include('scripts.datatables')--}}
    {{--    @include('scripts.delete-modal-script')--}}
@stop
