<?php

return [

    /*
     * DropZone CDN
     */
    'dropZoneJsCDN' => env('DROPZONE_JS_CDN', 'https://cdnjs.cloudflare.com/ajax/libs/dropzone/5.9.2/min/dropzone.min.js'),
];
